unitlength 1;           # 1mm
font Sans_Serif 4;

set radius [expr 93.7 / 2];  # Kugelradius
puts $radius
set thinline 0.15;
set thickline 0.4;
set overlap 3;
set spacerLen 20; # Länge der Abstandszapfen
set spacerWd 5;   # Durchmesser der Abstandszapfen
set spacerVis 5;  # sichtbare Länge der Abstandszapfen
set auxiliaryDiameter 3
set bracketRadius 75;
set bracketHight 50;
set bracketDiameter0 100;
set bracketDiameter1 50;
set bracketDiameter2 120;
set bracketFootWidth [expr $bracketRadius - $bracketDiameter2 / 2];
set bracketFootHeight 20;
 
set Pk [expr sqrt($radius * $radius / 2)];
set Pk_ [expr sqrt(($radius + 1)^2 / 2)];
set Ps1 [expr $Pk + sqrt($spacerLen * $spacerLen / 2)];
set Ps1_ [expr $Pk + sqrt(($spacerLen + 1)^2 / 2)];
set Ps2 [expr $Ps1 + sqrt($spacerVis * $spacerVis / 2)];
set P1 [expr sqrt(($radius + $spacerVis) * ($radius + $spacerVis) / 2)];
set P1_ [expr sqrt(($radius + $spacerVis + 1)^2 / 2)];
set P2 25
set P3  $bracketRadius

# Umrisse Edelstahlhalterung
pen black $thickline solid
moveto -$bracketRadius $P2
rectangle [expr $bracketRadius * 2] $bracketHight

moveto [expr -$bracketRadius + $bracketFootWidth] [expr $P2 + $bracketHight]
linerel 0 -$bracketFootHeight
linerel $bracketDiameter2 0 
linerel 0 $bracketFootHeight

moveto -$P2 [expr $P2 * 2]
linerel $bracketDiameter1 0

# Edelstahlhalterung (geschnitten) 
pen lightgray $thickline solid
fillpolygon -$bracketRadius $P2 \
  [expr -$bracketDiameter0 / 2] $P2 \
  [expr -$bracketDiameter1 / 2] [expr $bracketDiameter0 / 2] \
  [expr -$bracketDiameter1 / 2] [expr $bracketDiameter0 / 2 + 5] \
  [expr -$bracketDiameter2 / 2] [expr $bracketDiameter0 / 2 + 5] \
  [expr -$bracketDiameter2 / 2] [expr $bracketDiameter0 / 2 + 5 + $bracketFootHeight] \
  [expr -$bracketRadius] [expr $bracketDiameter0 / 2 + 5 + $bracketFootHeight] 
pen black $thickline solid
polygon -$bracketRadius $P2 \
  [expr -$bracketDiameter0 / 2] $P2 \
  [expr -$bracketDiameter1 / 2] [expr $bracketDiameter0 / 2] \
  [expr -$bracketDiameter1 / 2] [expr $bracketDiameter0 / 2 + 5] \
  [expr -$bracketDiameter2 / 2] [expr $bracketDiameter0 / 2 + 5] \
  [expr -$bracketDiameter2 / 2] [expr $bracketDiameter0 / 2 + 5 + $bracketFootHeight] \
  [expr -$bracketRadius] [expr $bracketDiameter0 / 2 + 5 + $bracketFootHeight] 

pen lightgray $thickline solid
fillpolygon $bracketRadius $P2 \
  [expr $bracketDiameter0 / 2] $P2 \
  [expr $bracketDiameter1 / 2] [expr $bracketDiameter0 / 2] \
  [expr $bracketDiameter1 / 2] [expr $bracketDiameter0 / 2 + 5] \
  [expr $bracketDiameter2 / 2] [expr $bracketDiameter0 / 2 + 5] \
  [expr $bracketDiameter2 / 2] [expr $bracketDiameter0 / 2 + 5 + $bracketFootHeight] \
  [expr $bracketRadius] [expr $bracketDiameter0 / 2 + 5 + $bracketFootHeight] 
pen black $thickline solid
polygon $bracketRadius $P2 \
  [expr $bracketDiameter0 / 2] $P2 \
  [expr $bracketDiameter1 / 2] [expr $bracketDiameter0 / 2] \
  [expr $bracketDiameter1 / 2] [expr $bracketDiameter0 / 2 + 5] \
  [expr $bracketDiameter2 / 2] [expr $bracketDiameter0 / 2 + 5] \
  [expr $bracketDiameter2 / 2] [expr $bracketDiameter0 / 2 + 5 + $bracketFootHeight] \
  [expr $bracketRadius] [expr $bracketDiameter0 / 2 + 5 + $bracketFootHeight] 

# Kugel
moveto 0 0
pen darkorange
fillcircle $radius

# Mittellinien
pen black $thinline dashdotted 
line 0 [expr $radius + 20 + $overlap] 0 [expr -$radius - $overlap]
line [expr $radius + $overlap] 0 [expr -$radius - $overlap] 0

# Entlüftungsbohrung
pen white solid
moveto  [expr -$Ps1] [expr $Ps1]
fillrod 15 $auxiliaryDiameter 135
pen black $thickline solid
moveto -57 55
linepolar 12.3 -45 
moveto -53 55
linepolar 9.5 -45
line [expr -$bracketDiameter2 / 2] [expr $bracketDiameter0 / 2 + 5] \
  [expr -$bracketDiameter1 / 2] [expr $bracketDiameter0 / 2 + 5]
# Zapfen
pen 0x97BD1B solid
moveto  [expr -$Ps1_] [expr $Ps1_]
fillrod $spacerLen $spacerWd -45
pen black $thickline solid
moveto  [expr -$Ps1_] [expr $Ps1_]
rod $spacerLen $spacerWd -45 

# Bemaßung mittlerer Durchmesser
pen black $thinline solid
dimline [geom.parallel [expr -$bracketDiameter1 / 2] $P2 \
  [expr $bracketDiameter1 / 2] $P2 37]  "ø%.0f"
# Bemaßung unterer Durchmesser
dimline [geom.parallel [expr -$bracketDiameter2 / 2] $P2 \
  [expr $bracketDiameter2 / 2] $P2 57]  "ø%.0f"
# Bemaßung Außendurchmesser  
dimline [geom.parallel -$bracketRadius $P2 $bracketRadius $P2 64]  "ø%.0f"

# Bemaßung oberer Durchmesser 
moveto [expr $bracketDiameter0 / 2] $P2
linerel 0 -75
moveto [expr -$bracketDiameter0 / 2] $P2
linerel 0 -75
dimline [geom.parallel [expr -$bracketDiameter0 / 2] $P2 \
  [expr $bracketDiameter0 / 2] $P2 -75]  "ø%.0f"

# Bemaßung der Bohrungsposition
moveto [expr -$bracketDiameter0 / 2] $P2
linepolar 20 -45
moveto -$Pk_ $Pk_ 
linepolar [expr 21 - $spacerVis] -45
dimline [geom.parallel [expr -$bracketDiameter0 / 2] $P2 \
  -$P1_ $P1_ -20] "%.0f"

# Bemaßung Höhe
dimline [geom.parallel $bracketRadius $P2 \
  $bracketRadius [expr $P2 + $bracketHight] -7]
  
# Bemaßung Ausdrehung
dimline [geom.parallel $bracketRadius [expr $P2 + $bracketHight - \
  $bracketFootHeight] $bracketRadius [expr $P2 + $bracketHight] 20]

moveto -77 0 
arrowto -42 38
moveto -74 -12  
label "Drei Bohrungen um" N
moveto -74 -8
label "jeweils 120° versetzt" N
moveto -77 0
label "ø5, 15 tief" N

# kleine Bohrung
moveto -90 10 
arrowto -53 51
moveto -90 10 
label "ø3" N

# Schräge
moveto 47 47
arrowto 37.5 37.5
moveto 47 47 
label "45°" NO

moveto -90 10

