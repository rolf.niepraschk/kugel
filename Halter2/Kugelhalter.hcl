# Rolf Niepraschk, Rolf.Niepraschk@ptb.de, 2020-09-09

# Drawj2d Program Reference: https://drawj2d.sourceforge.io/drawj2d_en.pdf

unitlength 1; # 1mm
font Sans_Serif 4;

set thinline 0.15;
set thickline 0.4;

# Wichtige Konstanten
set Rk [expr 93.7 / 2]; # Kugelradius
set R1 53;  # Außendradius Halter
set R2 32;  # Radius Ausdrehung unten  
set R3 27;  # Radius zentrale Bohrung
set R4 50;  # Radius Beginn der Schräge
set Ht 30;  # Höhe Halter
set Ph 45;  # Winkel der Schräge
set Pb 135; # Winkel der schrägen Bohrung
set Db 2;   # Durchmesser der schrägen Bohrung
set Dk 4;   # Durchmesser der Kunststoffkappe
set Lk 6;   # Länge der Kunststoffkappe
set Cc 0x97BD1B; # Farbe der Kappe
set As 18;  # Abstand der schrägen Bohrung von Oberkante (Pos B) 
set Az 3 ;  # Höhe Ausdrehung unten
set Tb 10;  # Tiefe der schrägen Bohrung
set Lb [expr $Tb + 4]; # Länge des Stiftes
set Ck darkorange; # Farbe der Kugel
set Ol 3;   # Überstand der Mittellinien
set Ma 8;   # Abstand erste Maßlinie
set Mb 5;   # Abstand weiterer Maßlinien
set Po 120; # Winkelabstand der schrägen Bohrungen

# Markante Punkte
set O {0 0}; # Ursprung
set A "$R1 0"; # Punkt rechts oben
set B "$R4 0"; # Punkt Beginn der Schräge
set C "$R3 [expr $R4 - $R3]"; # Punkt Ende der Schräge
set D "$R3 $Ht"; # Punkt am Ende der zentralen Bohrung
set E "$R2 $Ht"; # Punkt am Ende der Ausdrehung
set F "$R2 [expr $Ht - $Az]"; # Punkt am Anfang der Ausdrehung
set G "$R1 [Y $F]"; # Punkt rechts unten
# Beginn schräge Bohrung
set P "[expr sqrt($As^2 / 2) - $R4] [expr sqrt($As^2 / 2)]";
set tmp [expr $Lb - $Tb];
# Beginn Stift
set Q "[expr [X $P] + sqrt($tmp^2 / 2)] [expr [Y $P] - sqrt($tmp^2 / 2)]";
# Ende Stift
set tmp [expr sqrt($Tb^2 / 2)]
set S "[expr [X $P] - $tmp] [expr [Y $P] + $tmp]"; 
# Beginn Kappe
set R "[expr [X $P] + sqrt($Lk^2 / 2)] [expr [Y $P] - sqrt($Lk^2 / 2)]"; 
set K "0 -25"; # Position der Kugelmitte

# Geschnittene Struktur
proc drawSectional {f} {
  global thickline; 
  global A; global B; global C; global D; global E; global F; global G;
  block
    block.scale $f 1
    pen lightgray $thickline solid
    fillpolygon $A $B $C $D $E $F $G
    pen black $thickline solid
    polygon $A $B $C $D $E $F $G
  endblock
}
lineto $A -$A; lineto $C -$C; lineto $E -$E;
moveto $O;  
drawSectional  1; # rechte Seite
drawSectional -1; # linke Seite (gespiegelte rechte Seite)

# Kappe
pen $Cc $thickline solid
moveto $R; fillrod $Lk $Dk $Pb
pen black $thinline solid  
moveto $R; rod $Lk $Dk $Pb
 
# Stift
pen black $thickline solid 
moveto $Q; fillrod $Lb $Db $Pb

# Kugel
moveto $K 
pen $Ck $thickline solid
fillcircle $Rk

# Bemaßungen
pen black $thinline solid

# Außendurchmesser
dimline [geom.parallel -$G $G [expr $Az + $Ma + 2 * $Mb]] "ø%.0f";

# Ausdrehung 
dimline [geom.parallel -$E $E [expr $Ma + $Mb]] "ø%.0f";

# Durchgangsbohrung
dimline [geom.parallel -$D $D $Ma] "ø%.0f"; 

# oberer Durchmesser 
set tmp "[X $B] [expr [Y $K] - $Rk - $Mb]"
line $B $tmp
set tmp "[X -$B] [expr [Y -$K] - $Rk - $Mb]"
line -$B $tmp
dimline [here] "[expr [X [here]] + 2 * $R4] [Y [here]]" "ø%.0f";

# Abstand schräge Bohrung
moveto -$B
linepolar $Tb $Pb 
dimline [geom.parallel [here] $S [expr $Mb / 3]] "%.0f"; 


# Höhe des Halters
dimline [geom.parallel $A "[X $A] [Y $E]" -$Ma] "%.0f";

# Ausdrehung
set tmp "[X $E] [expr [Y $E] - ([Y $E] - [Y $F]) / 2]"
moveto "[expr [X $tmp] + 5] [expr [Y $tmp] + 5]"; label "$Az" SE
arrowto "[X $tmp] [Y $tmp]"; 

# Schräge Bohrung
moveto -70 34
text "Drei Bohrungen um jeweils $Po° versetzt, ø$Db, $Tb tief" 40 left

# Schräge
###moveto "[expr -[X $P] + 5] [expr [Y $P] + 5]"; label "$Ph°" SE
###arrowto "[expr -[X $P]] [Y $P]"
dimangle $C "[expr -[X $C]] [Y $C]" $B; 

# Mittellinien
pen black $thinline dashdotted
line 0 [expr $Ht + $Ol] 0 [expr [Y $K] - $Rk - $Ol]
moveto [expr [X $K] + $Rk + $Ol] [Y $K]
linerel [expr - 2 * $Rk - 2 * $Ol] 0

